var gulp       = require('gulp');
var mocha      = require('gulp-mocha');
var eslint     = require('gulp-eslint');

gulp.task('lint', function() {
  return gulp.src(['src/**/*.js'])
    .pipe(eslint())
    .pipe(eslint.format())
    .pipe(eslint.failAfterError());
});

gulp.task('test', function() {
  return gulp.src(['test/**/*.test.js'])
    .pipe(mocha());
});

gulp.task('watch', function() {
  gulp.watch(['src/**/*.js'], ['lint', 'test']);
})

gulp.task('default', ['lint', 'test', 'watch']);
