'use strict';

// imports
let optimist       = require('optimist');
let redditComments = require('./comments');


// setup cli interface
let args = optimist
	.usage('Usage: $0 --u[sername] string --p[assword] --s[ubreddit] string --f[ile] string --d[ebug] bool')
	.default('subreddit', 'all')
	.default('debug', false)
	.default('file', 'codes.dat')
	.alias('u', 'username')
  .describe('u', 'Reddit username')
  .alias('p', 'password')
  .describe('p', 'Reddit password')
	.alias('s', 'subreddit')
  .describe('s', 'Subreddit to crawl. Defaults to /r/all')
	.alias('f', 'file')
  .describe('f', 'File to dump results')
	.alias('d', 'debug')
  .describe('d', 'Enable debugging')
  .demand('username')
  .demand('password')
  .argv;

// cli help
if (args.h || args.help) {
	console.log(optimist.help());
	return void 0;
}

// parse arguments list
let sub = args.subreddit ? args.subreddit : null;
let file = args.file ? args.file : null;
let debug = args.debug;

// api auth details
let appAuth = { id: '#########', secret: '##########' };
let comments = new redditComments(args.u, args.p, appAuth, sub, file, debug);

// lets go
comments.start();