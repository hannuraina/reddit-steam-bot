'use strict';

let _            = require('lodash');
let fs           = require('fs');
let RedditStream = require('reddit-stream');

class Comments {
  constructor(usr, pwd, app, sub, file, debug) {
    this.bot       = 'hannuraina-bot/1.0';
    this.auth      = { username: usr, password: pwd, app: app };
    this.sub       = sub   || 'all';
    this.file      = file  || 'data';
    this.debug     = debug || false;
    this.test      = /\s((?:\w{4,5}-){2,4}\w{4,5}\W)/ig;
    this.stream    = new RedditStream('comments', this.sub, this.bot);
  };

  parse(comments) {
    _.each(comments, (comment) => {
      if (comment.data.body && this.test.test(comment.data.body)) {
        _.each(comment.data.body.match(this.test), (code) => {
          let link = [comment.data.link_url, comment.data.id].join('');
          this.dump([
            '', '/r/' + comment.data.subreddit,
            code.trim(),
            link, ''
          ].join('\r\n'));
        });
      }
    })
  }

  dump(data) {
    let log = fs.createWriteStream(this.file, {'flags': 'a'});
    log.once('open', () => {
      this.debug ? console.log(data) : void 0;
      log.write(data);
      log.end();
    });
  }

  start() {
    this.stream.on('login.success', (access) => { this.debug ? console.log('Loggin in:', access.access_token) : void 0 });
    this.stream.on('login.error', () => { console.log('Login Failed') });
    this.stream.on('new', this.parse.bind(this));

    this.stream.login(this.auth).then((access) => {
      return Promise.resolve(this.stream.start());
    });
  }
};


module.exports = Comments;
