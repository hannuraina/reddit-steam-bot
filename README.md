# Reddit Steam Bot

Crawl through specified subreddit for steam/xbl/psn codes.

### Version
0.0.1

### Installation

Requires NodeJS. Install from [nodejs.org](https://nodejs.org/en/download/stable/)

```sh
$ git clone https://bitbucket.org/hannuraina/reddit-steam-bot reddit-steam-bot
$ cd reddit-steam-bot
$ npm install
```

Requires reddit oAuth key and secret. You can request oAuth key from [reddit.com](https://www.reddit.com/prefs/apps).
Then update the following line in src/index.js

```javascript
40: let appAuth = { id: 'my-app-id', secret: 'my-app-secret' };
```

### Running

```sh
$ node src/index.js 
```

### CLI Options
```sh
Options:
  -s, --subreddit  Subreddit to crawl. Defaults to /r/all
  -f, --file       File to dump results
  -d, --debug      Enable debugging
  --subreddit                                              [default: "all"]
  --debug                                                  [default: false]
  --file                                                   [default: "data"]
```

### Examples
Search /r/all with debug on:
```sh
$ node src/index.js --username=hannuraina --password=mypass --subreddit =all --debug
```

Search /r/foo with debug on and write to foo.dat:
```sh
$ node src/index.js --username=hannuraina --password=mypass --s=foo --d --file=foo.dat 
```

Search /r/gaming and write to codes.dat:
```sh
$ node src/index.js --username=hannuraina --password=mypass --s=gaming --file=codes.dat
```

License
----

MIT
